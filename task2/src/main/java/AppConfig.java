import beans.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfig {

    @Bean
    public Car car() {
        List<Wheel> wheels = new ArrayList<>();
        wheels.add(wheel());
        wheels.add(wheel());
        wheels.add(wheel());
        wheels.add(wheel());
        return new Car(wheels, engine());
    }

    @Bean
    @Scope("prototype")
    public Wheel wheel() {
        return new Wheel(winterTyres());
    }

    @Bean
    public Engine engine() {
        return new Engine(300);
    }

    @Bean
    public WinterTyres winterTyres() {
        return new WinterTyres(19, "Bridgestone");
    }

    @Bean
    public SummerTyres summerTyres() {
        return new SummerTyres(19, "Nokian");
    }
}
