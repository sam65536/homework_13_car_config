import beans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        Car car = context.getBean(Car.class);
        Engine engine = car.getEngine();
        List<Wheel> wheels = car.getWheels();
        System.out.println("Car with Engine capacity: " + engine.getEngineCapacity());
        System.out.println("Car with " + wheels.size() + " wheels");
        for (int i=0; i < wheels.size(); i++) {
            Tyres tyre = wheels.get(i).getTyres();
            System.out.println((i+1) + "st " + "wheel with tyre " + tyre.getName() + ", size " + tyre.getSize());
        }
        Tyres tyres = context.getBean(SummerTyres.class);
        System.out.println();
        for (int i=0; i < wheels.size(); i++) {
            wheels.get(i).setTyres(tyres);
            System.out.println((i+1) + "st " + "wheel with tyre " + tyres.getName() + ", " + "size " + tyres.getSize());
        }
    }
}