import beans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("beans");
        Car car = context.getBean(Car.class);
        Engine engine = car.getEngine();
        engine.setEngineCapacity(300);
        List<Wheel> wheels = car.getWheels();
        wheels.add(context.getBean(Wheel.class));
        wheels.add(context.getBean(Wheel.class));
        wheels.add(context.getBean(Wheel.class));

        WinterTyres winterTyres = (WinterTyres) context.getBean("winterTyres");
        winterTyres.setName("Bridgestone");
        winterTyres.setSize(19);
        for (int i=0; i < wheels.size(); i++) {
            wheels.get(i).setTyres(winterTyres);
        }

        System.out.println("Car with Engine capacity: " + engine.getEngineCapacity());
        System.out.println("Car with " + wheels.size() + " wheels");
        for (int i=0; i < wheels.size(); i++) {
            Tyres tyre = wheels.get(i).getTyres();
            System.out.println((i+1) + "st " + "wheel with tyre " + tyre.getName() + ", size " + tyre.getSize());
        }

        SummerTyres summerTyres = (SummerTyres) context.getBean("summerTyres");
        summerTyres.setName("Nokian");
        summerTyres.setSize(19);
        for (int i=0; i < wheels.size(); i++) {
            wheels.get(i).setTyres(summerTyres);
        }

        System.out.println();
        for (int i=0; i < wheels.size(); i++) {
            Tyres tyre = wheels.get(i).getTyres();
            System.out.println((i+1) + "st " + "wheel with tyre " + tyre.getName() + ", " + "size " + tyre.getSize());
        }
    }
}